from astropy.io import fits
import pandas as pd
import os
import subprocess
import time
from pathlib import Path

DOCKER_COMMAND = "docker run --rm -v /home/narek/Desktop/blazar/new_vou_blazar/work_dir:/work_dir vzgo666/vou_amd64"
DOWNLOADED_FILE_PATH = Path("/home/narek/Desktop/blazar/new_vou_blazar/") / "work_dir"
FITS_DATA = "table-4LAC-DR3-h.fits"

hdul = fits.open(FITS_DATA)
data = hdul[1].data
df = pd.DataFrame(data)

df = df.apply(lambda col: pd.Series(col.values.byteswap().newbyteorder()))

for index, row in df.iterrows():
    ra = row['RA_Counterpart']
    dec = row['DEC_Counterpart']
    source_name = row['Source_Name']
    source_name = source_name.replace(" ", "_")

    cmd = DOCKER_COMMAND + f" --ra {ra} --dec {dec}"
    subprocess.run(cmd, shell=True)

    if dec < 0:
        downloaded_file_name = DOWNLOADED_FILE_PATH / f"{ra}_m{abs(dec)}"
    else:
        downloaded_file_name = DOWNLOADED_FILE_PATH / f"{ra}_{abs(dec)}"

    new_file_name = DOWNLOADED_FILE_PATH / f"{source_name}-SED"

    os.rename(f"{downloaded_file_name}.csv", f"{new_file_name}.csv")
    os.rename(f"{downloaded_file_name}.txt", f"{new_file_name}.txt")

    time.sleep(0.1)
